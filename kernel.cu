#include <stdio.h>

// kernel function 1
__global__ void kernel1(int *d_ptr1) {
    int idx = threadIdx.x + blockIdx.x * blockDim.x;
    printf("hello kernel1 from idx = %d, d_ptr1 = %d\n", idx, d_ptr1[idx]);
}

// kernel function 2
__global__ void kernel2(int *d_ptr2) {
    int idx = threadIdx.x + blockIdx.x * blockDim.x;
    printf("hello kernel2 from idx = %d, d_ptr2 = %d\n", idx, d_ptr2[idx]);
}

// kernel function 3
__global__ void kernel3(int *d_ptr3) {
    int idx = threadIdx.x + blockIdx.x * blockDim.x;
    printf("hello kernel3 from idx = %d, d_ptr3 = %d\n", idx, d_ptr3[idx]);
}

int main() {
    // var init
    int *h_ptr1, *h_ptr2, *h_ptr3;
    int *d_ptr1, *d_ptr2, *d_ptr3;
    size_t cudaSize =  sizeof(int);

    // mem alloc
    h_ptr1 = (int*) calloc(1, sizeof(int));
    h_ptr2 = (int*) calloc(1, sizeof(int));
    h_ptr3 = (int*) calloc(1, sizeof(int));
    h_ptr1[0] = 1; h_ptr2[0] = 22; h_ptr3[0] = 333;
    cudaMalloc(&d_ptr1, cudaSize);
    cudaMalloc(&d_ptr2, cudaSize);
    cudaMalloc(&d_ptr3, cudaSize);

    // cuda copy hose to device
    cudaMemcpy(d_ptr1, h_ptr1, cudaSize, cudaMemcpyHostToDevice);
    cudaMemcpy(d_ptr2, h_ptr2, cudaSize, cudaMemcpyHostToDevice);
    cudaMemcpy(d_ptr3, h_ptr3, cudaSize, cudaMemcpyHostToDevice);

    // kernel launches
    kernel1<<< 1, 1 >>>(d_ptr1);
    kernel2<<< 1, 1 >>>(d_ptr2);
    kernel3<<< 1, 1 >>>(d_ptr3);

    // cuda copy device to host
    cudaMemcpy(h_ptr1, d_ptr1, cudaSize, cudaMemcpyDeviceToHost);
    cudaMemcpy(h_ptr2, d_ptr2, cudaSize, cudaMemcpyDeviceToHost);
    cudaMemcpy(h_ptr3, d_ptr3, cudaSize, cudaMemcpyDeviceToHost);

    // cleanup memory
    cudaFree(d_ptr1);
    cudaFree(d_ptr2);
    cudaFree(d_ptr3);

    return 0;
}
