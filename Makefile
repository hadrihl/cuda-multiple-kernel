CUDA_CC = nvcc
CUDA_FLAGS = -arch=sm_20 -m64

BIN = a.out

all: $(BIN)

a.out: kernel.cu
	$(CUDA_CC) $(CUDA_FLAGS) -o $@ $<

clean:
	$(RM) $(BIN)
